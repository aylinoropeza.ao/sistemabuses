@extends('adminlte::page')

@section('title', 'Asientos')

@section('content_header')
    <h1>Asientos</h1>
@stop

@section('content')
    <a href="/admin/asientos/create?idbus={{ request()->get('idbus') }}" class="btn btn-primary">Registrar Asientos</a>
      <table class="table table-dark table-striped mt-4">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nro Asiento</th>
            <th scope="col">Fila</th>
            <th scope="col">Columna</th>
            <th scope="col">Gestion</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($asientos as $asiento)
          <tr>
            <td>{{  $loop->index + 1 }}</td>
            <td>{{$asiento->num_asiento}}</td>
            <td>{{$asiento->fila}}</td>
            <td>{{$asiento->columna}}</td>
            <td>
              <form action="{{route('asientos.destroy', $asiento->id)}}" method="POST">
                <input type="hidden" name="bus_id" value="{{ request()->get('idbus') }}">
                <a href="/admin/asientos/{{$asiento->id}}/edit?idbus={{ request()->get('idbus') }}" class="btn btn-info">Editar</a>
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Eliminar</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop
