
@extends('adminlte::page')
@section('title', 'Editar asiento')
@section('content')
<div class="container">
    <div class="card">
        <h4>EDITAR REGISTRO</h4>
        <form class="row g-3" action="/admin/asientos/{{$asiento->id}}" method="POST">
        @csrf
        @method('PUT')
            <div class="col-md-6">
              <label for="num_asiento" class="form-label">Nro Asiento</label>
              <input type="text" name="num_asiento" class="form-control" id="num_asiento" value="{{$asiento->num_asiento}}">
              <input type="hidden" name="bus_id" value="{{ request()->get('idbus') }}">
            </div>
            <div class="col-md-6">
              <label for="fila" class="form-label">Fila</label>
              <input type="text" name="fila" class="form-control" id="fila" value="{{$asiento->fila}}">
            </div>
            <div class="col-12">
              <label for="columna" class="form-label">Columna</label>
              <input type="text" name="columna" class="form-control" id="columna" value="{{$asiento->columna}}">
            </div>
            <div class="col-12">
            <a href="/admin/asientos" class="btn btn-secondary">Cancelar</a>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
