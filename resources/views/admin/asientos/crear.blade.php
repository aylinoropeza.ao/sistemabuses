
@extends('adminlte::page')
@section('title', 'Añadir asientos')
@section('content')
<div class="container">
    <div >
        <h4>Registrar asientos</h4>
        <form class="row g-3" action="/admin/asientos" method="POST">
        @csrf
            <div class="col-md-6">
              <label for="num_asiento" class="form-label">Nro Asiento</label>
              <input type="text" name="num_asiento" class="form-control" id="num_asiento">
              <input type="hidden" name="bus_id" value="{{ request()->get('idbus') }}">
            </div>
            <div class="col-md-6">
              <label for="fila" class="form-label">Nro Fila</label>
              <input type="number" name="fila" class="form-control" id="fila">
            </div>
            <div class="col-12">
              <label for="columna" class="form-label">Nro Columna</label>
              <input type="text" name="columna" class="form-control" id="columna">
            </div>
            <div class="col-auto">
              </div>
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
