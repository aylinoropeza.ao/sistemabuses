<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style>
    table {
      border-collapse: collapse;
      width: 100%;
    }
    
    th, td {
      text-align: left;
      padding: 8px;
    }
    
    tr:nth-child(even){background-color: #f2f2f2}
    
    th {
      background-color: #04AA6D;
      color: white;
    }
    div {
      padding: 50px;
    }
    </style>
</head>
<body>
  @if (session('status'))
    <div class="alert alert-success" role="alert">
      {{ session('status') }}
    </div>
  @endif
<div>
  <h1>REPORTE DE VIAJES</h1>
  <table>
    <thead>
      <tr>
        <th scope="col">ID venta</th>
        <th scope="col">ID boleto</th>
        <th scope="col">Nombre</th>
        <th scope="col">Apellido</th>
        <th scope="col">N° de asiento</th>
        <th scope="col">Origen</th>
        <th scope="col">Destino</th>
        <th scope="col">Tipo de bus</th>
        <th scope="col">Placa</th>
        <th scope="col">Hora del viaje</th>
        <th scope="col">Fecha del viaje</th>
        <th scope="col">Pago</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($detalles as $detalle)
        <tr>
          <td>{{ $detalle->venta_id }}</td>
          <td>{{ $detalle->id }}</td>
          <td>{{ $detalle->nombre }}</td>
          <td>{{ $detalle->apellido }}</td>
          <td>{{ $detalle->num_asiento }}</td>
          <td>{{ $detalle->origen }}</td>
          <td>{{ $detalle->destino }}</td>
          <td>{{ $detalle->tipo }}</td>
          <td>{{ $detalle->placa }}</td>
          <td>{{ $detalle->hora_partida }}</td>
          <td>{{ $detalle->fecha_partida }}</td>
          <td>{{ $detalle->precio }}</td>
        </tr>
    @endforeach
    </tbody>
  </table>

</div>
</body>
</html>