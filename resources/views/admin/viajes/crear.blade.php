@extends('adminlte::page')
@section('title', 'Añadir viaje')
@section('content')
<div>
    <div>
        <h2>REGISTRAR VIAJES</h2>
        <form class="row g-3" action="/admin/viajes" method="POST">
        @csrf
            <div class="col-md-6">
              <label for="hora" class="form-label">Hora de partida</label>
              <input type="time" name="hora_partida" class="form-control" id="hora">
            </div>
            <div class="col-md-6">
              <label for="fecha" class="form-label">Fecha de partida</label>
              <input type="date" name="fecha_partida" class="form-control" id="fecha">
            </div>
            <div class="col-md-6 my-4">
              <label for="fecha" class="form-label">Tramo</label>
              <select name="tramo_id" id="" class="p-2">
                @foreach ($tramos as $tramo)
                  <option value="{{$tramo->id}}">{{$tramo->origen}} -  {{$tramo->destino}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-auto my-4">
                <label class="visually-hidden" for="nro">Placa</label>
                <select class="form-select p-2" name="bus_id" id="">
                @foreach ($buses as $bus)
                  <option value="{{$bus->id}}">{{$bus->placa}}</option>
                @endforeach
                </select>
              </div>
              <div class="col-md-6">
              <label for="precio" class="form-label">Precio</label>
              <input type="number" name="precio" class="form-control" id="precio">
            </div>
            <div class="col-12 py-4">
              <a href="/admin/viajes" class="btn btn-secondary" tabindex="3">Cancelar</a>
              <button type="submit" class="btn btn-secondary">Guardar</button>
            </div>
          </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
