@extends('adminlte::page')

@section('title', 'viajes')

@section('content_header')
    <h1>Viajes</h1>
@stop

@section('content')

    <a href="/admin/viajes/create" class="btn btn-primary">Registrar viajes</a>
    <table class="table table-dark table-striped mt-4">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Hora de partida</th>
                <th scope="col">Fecha de partida</th>
                <th scope="col">Tramo</th>
                <th scope="col">Placa</th>
                <th scope="col">Precio</th>
                <th scope="col">Espacios</th>
                <th scope="col">Gestionar Viajes</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($viajes as $viaje)
              <tr>
                <td>{{  $loop->index + 1 }}</td>
                <td>{{$viaje->hora_partida}}</td>
                <td>{{$viaje->fecha_partida}}</td>
                <td>{{$viaje->tramo}}</td>
                <td>{{$viaje->bus}}</td>
                <td>{{$viaje->precio}}</td>
                <td><a href="/admin/espacios?id={{$viaje->id}}" class="btn btn-info">Espacios</a></td>
                <td>
                      <form action="{{route('viajes.destroy', $viaje->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/admin/viajes/{{$viaje->id}}/edit" class="btn btn-info">Editar</a>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                      </form>
                </td>
              </tr>
            @endforeach
            </tbody>

          </table>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css"> 
@stop
