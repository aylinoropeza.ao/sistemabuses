@extends('adminlte::page')
@section('title', 'Editar viaje')
@section('content')
<div>
    <div>
        <h2>EDITAR REGISTRO</h2>
        <form class="row g-3" action="/admin/viajes/{{$viaje->id}}" method="POST">
        @csrf
        @method('PUT')
            <div class="col-md-6">
              <label for="hora" class="form-label">Hora de partida</label>
              <input type="time" value="{{$viaje->hora_partida}}" name="hora_partida" class="form-control" id="hora">
            </div>
            <div class="col-md-6">
              <label for="fecha" class="form-label">Fecha de partida</label>
              <input type="date" value="{{$viaje->fecha_partida}}" name="fecha_partida" class="form-control" id="fecha">
            </div>
            <div class="col-md-6">
              <label for="fecha" class="form-label">Tramo</label>
              <select name="tramo_id" id="">
                @foreach ($tramos as $tramo)
                  <option {{ !! $tramo->id == $viaje->tramo_id ? 'selected="true"':''}}
                    value="{{$tramo->id}}">{{$tramo->origen}} -  {{$tramo->destino}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="nro">Placa</label>
                <select class="form-select" name="bus_id">
                @foreach ($buses as $bus)
                  <option 
                    {{ !! $bus->id == $viaje->bus_id ? 'selected="true"':''}} 
                    value="{{$bus->id}}">{{$bus->placa}}</option>
                @endforeach
                </select>
              </div>
              <div class="col-md-6">
              <label for="precio" class="form-label">Precio</label>
              <input type="number" name="precio" value="{{$viaje->precio}}" class="form-control" id="precio">
            </div>
            <div class="col-12">
              <a href="/admin/viajes" class="btn btn-secondary" tabindex="3">Cancelar</a>
              <button type="submit" class="btn btn-secondary" >Guardar</button>
            </div>
          </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop