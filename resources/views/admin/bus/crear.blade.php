
@extends('adminlte::page')
@section('title', 'Añadir Buses')
@section('content')
<div >
    <div>
        <h4>Registrar buses</h4>
        <form class="row g-3" action="/admin/bus" method="POST">
        @csrf
            
            <div class="col-md-6">
              <label for="nrofila" class="form-label">Nro Filas</label>
              <input type="number" name="n_filas" class="form-control" id="nrofila">
            </div>
            <div class="col-md-6">
              <label for="nrocolumna" class="form-label">Nro Columnas</label>
              <input type="number" name="n_columnas" class="form-control" id="nrocolumna">
            </div>
            <div class="col-12">
              <label for="placa" class="form-label">Placa</label>
              <input type="text" name="placa" class="form-control" id="placa">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="tipo">Tipo</label>
                <select class="form-select" name="tipo" id="tipo">
                  <option selected>Seleccionar...</option>
                  <option value="normal">Normal</option>
                  <option value="cama">Cama</option>
                  <option value="semi cama">Semi Cama</option>
                </select>
              </div>
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
