@extends('adminlte::page')

@section('title', 'bus')

@section('content_header')
    <h1>Buses</h1>
@stop

@section('content')

    <a href="/admin/bus/create" class="btn btn-primary">Registrar Buses</a>
        <table class="table table-dark table-striped mt-4">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tipo</th>
                <th scope="col">Placa</th>
                <th scope="col">Nro Filas</th>
                <th scope="col">Nro Columnas</th>
                <th scope="col">Asientos</th>
                <th scope="col">Gestion</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($buses as $bus)
              <tr>
                <td>{{$bus->id}}</td>
                <td>{{$bus->tipo}}</td>
                <td>{{$bus->placa}}</td>
                <td>{{$bus->n_filas}}</td>
                <td>{{$bus->n_columnas}}</td>
                <td>
                <a href="/admin/asientos?idbus={{$bus->id}}" class="btn btn-info">Asientos</a>
                </td>
                <td>
                  <form action="{{route('bus.destroy', $bus->id)}}" method="POST">
                    <a href="/admin/bus/{{$bus->id}}/edit" class="btn btn-info">Editar</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                  </form>
                </td>
              </tr>
          @endforeach
            </tbody>
        </table> 
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop
