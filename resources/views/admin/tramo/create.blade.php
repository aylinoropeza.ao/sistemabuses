@extends('adminlte::page')

@section('title', 'Añadir tramo')

@section('content')

<h2>NUEVO TRAMO</h2>
<form action="/admin/tramo" method="POST">
  @csrf
  <div class="mb-6">
    <label for="" class="form-label">Lugar de partida: </label>
    <input id="origen" name="origen" type="text" class="form-control" tabindex="1">
  </div>
  <div class="mb-6">
    <label for="" class="form-label">Lugar de llegada: </label>
    <input id="destino" name="destino" type="text" class="form-control" tabindex="2">
  </div><br>
  <div>
    <a href="/admin/tramo" class="btn btn-secondary" tabindex="3">Cancelar</a>
    <button type="submit" class="btn btn-secondary" tabindex="4">Guardar</button>
  </div>
</form>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
