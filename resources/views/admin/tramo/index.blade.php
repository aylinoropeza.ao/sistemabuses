@extends('adminlte::page')

@section('title', 'Tramos')

@section('content_header')
    <h1>Tramos</h1>
@stop

@section('content')

<a href="tramo/create" class="btn btn-primary">Nuevo tramo</a>
<table class="table table-dark table-striped mt-4">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Origen</th>
      <th scope="col">Destino</th>
      <th scope="col">Operaciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($tramos as $tramo)
        <tr>
          <td>{{$tramo->id}}</td>
          <td>{{$tramo->origen}}</td>
          <td>{{$tramo->destino}}</td>
          <td>
            <form action="{{route('tramo.destroy', $tramo->id)}}" method="POST">
              <a href="/admin/tramo/{{$tramo->id}}/edit" class="btn btn-info">Editar</a>
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Eliminar</button>
            </form>
          </td>
        </tr>
    @endforeach
  </tbody>
</table>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop