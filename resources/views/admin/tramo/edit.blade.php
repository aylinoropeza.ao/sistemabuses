@extends('adminlte::page')

@section('title', 'Editar tramo')

@section('content')

<h2>EDITAR REGISTRO</h2>
<form action="/admin/tramo/{{$tramo->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="mb-6">
    <label for="" class="form-label">Lugar de partida: </label>
    <input id="origen" name="origen" type="text" class="form-control" value="{{$tramo->origen}}">
  </div>
  <div class="mb-6">
    <label for="" class="form-label">Lugar de llegada: </label>
    <input id="destino" name="destino" type="text" class="form-control" value="{{$tramo->destino}}">
  </div><br>
  <div>
    <a href="/admin/tramo" class="btn btn-secondary">Cancelar</a>
    <button type="submit" class="btn btn-secondary">Guardar</button>
  </div>
</form>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
