@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Registro de ventas</h1>
@stop

@section('content')
    <a href="admin/reporte" class="btn btn-primary">Descargar PDF</a>
<table class="table table-dark table-striped mt-4">
  <thead>
    <tr>
      <th scope="col">ID venta</th>
      <th scope="col">ID boleto</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">N° de asiento</th>
      <th scope="col">Origen</th>
      <th scope="col">Destino</th>
      <th scope="col">Tipo de bus</th>
      <th scope="col">Placa</th>
      <th scope="col">Hora del viaje</th>
      <th scope="col">Fecha del viaje</th>
      <th scope="col">Pago</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($detalles as $detalle)
        <tr>
          <td>{{ $detalle->venta_id }}</td>
          <td>{{ $detalle->id }}</td>
          <td>{{ $detalle->nombre }}</td>
          <td>{{ $detalle->apellido }}</td>
          <td>{{ $detalle->num_asiento }}</td>
          <td>{{ $detalle->origen }}</td>
          <td>{{ $detalle->destino }}</td>
          <td>{{ $detalle->tipo }}</td>
          <td>{{ $detalle->placa }}</td>
          <td>{{ $detalle->hora_partida }}</td>
          <td>{{ $detalle->fecha_partida }}</td>
          <td>{{ $detalle->precio }}</td>
        </tr>
    @endforeach
  </tbody>
</table>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
