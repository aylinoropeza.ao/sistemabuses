@extends('adminlte::page')

@section('title', 'Espacios de viajes')

@section('content_header')
    <h1>Espacios de viajes</h1>
@stop

@section('content')

<style> 
      .espacios-bus {
        position: relative;
        display: grid;
        grid-gap: 20px;
        background-color: white;
        padding: 20px;
        border-radius: 20px;
        max-width: max-content;
        margin: auto;
      }
      .espacios-bus > div {
        border-radius: 6px;
        height: 100px;
        font-size: 25px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .espacio-libre {
        background-color: #dddddd;
      }
      .espacio-ocupado {
        background-color: #13ad58;
        color: white;
      }
      .espacios-bus-col-4 {
        grid-template-columns: 80px 80px 80px 80px;
      }
      .espacios-bus-col-3 {
        grid-template-columns: 80px 80px 80px;
      }
      </style>
      <div>
      
      <!-- <h2>Bus {{$viaje->tipo}}</h2> -->
      </div>
    <div class="{{$viaje->n_columnas == 3 ? 'espacios-bus espacios-bus-col-3' : 'espacios-bus espacios-bus-col-4'}}" >
        @foreach ($espacios as $espacio)
            <div class="{{$espacio->estado == 'ocupado' ? 'espacio-ocupado':'espacio-libre' }}">{{$espacio->num_asiento}}</div>
        @endforeach
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
