@extends('template.nav')
@section('contenido')
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-end">
                <div class="col-6">
                    <img src="{{ asset('assets/img/bus.webp') }}" alt="" data-pagespeed-url-hash="1758797763"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                </div>
                <div class="col-lg-6">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h5 class="text-dark">La mejor manera de viajar</h5>
                            <h1 class="text-dark">Descubra los viajes</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- banner part start-->
    <section class="search_your_country">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="search_form">
                        <form action="/destinos" method="get">
                            <div class="form-row">
                                <form class="form-contact contact_form" action="/destinos" method="get"
                                    id="contactForm" novalidate="novalidate">
                                    <div class="row">
                                        
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="fecha">Fecha salida</label>
                                                <input class="form-control" name="fecha" id="fecha" type="date"
                                                    onfocus="this.placeholder = ''"
                                                    onblur="this.placeholder = 'Enter email address'"
                                                    placeholder='Enter email address' required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="origen">Origen</label>
                                                <input class="form-control" name="origen" id="origen" type="text"
                                                    onfocus="this.placeholder = ''"
                                                    onblur="this.placeholder = 'Enter Subject'" placeholder='Origen'>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="destino">Destino</label>
                                                <input class="form-control" name="destino" id="destino" type="text"
                                                    onfocus="this.placeholder = ''"
                                                    onblur="this.placeholder = 'Enter Subject'" placeholder='Destino'>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <button type="submit" class="button button-contactForm btn_1">Buscar<i
                                                class="flaticon-right-arrow"></i> </button>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection