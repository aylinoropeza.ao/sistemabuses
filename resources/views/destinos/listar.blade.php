@extends('template.background')

@section('contenido2')
   <div class="mi-bg container-fluid">
      <div class="row ml-5">
         <h2 class="col">Resultados</h2>
      </div>
      <div>
         @foreach ($resultados as $resultado)
            <div class="card col shadow my-2">
               <div class="card-body row">
                  <div class="col-2">
                     <h3>Tipo de bus</h3>
                     <p  class="h4 text-dark"><small>{{ $resultado->tipo }}</small></p>
                  </div>
                  <div class="col-2">
                     <h3>Placa</h3>
                     <p  class="h4 text-dark"><small>{{ $resultado->placa }}</small></p>
                  </div>
                  <div class="col-2">
                     <h3>Fecha de salida</h3>
                     <p  class="h4 text-dark"><small>{{ $resultado->fecha_partida }}</small></p>
                  </div>
                  <div class="col-2">
                     <h3>Origen</h3>
                     <p  class="h4 text-dark"><small>{{ $resultado->origen }}</small></p>
                  </div>
                  <div class="col-2">
                     <h3>Destino</h3>
                     <p  class="h4 text-dark"><small>{{ $resultado->destino }}</small></p>
                  </div>
                  <div class="col-2">
                     <h3>Precio</h3>
                     <a href="/compra/{{ $resultado->id }}" class="btn btn-success">{{ $resultado->precio }} Bs. </a>
                  </div>
               </div>
            </div>
         @endforeach
      </div>
   </div>
@endsection
