<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        th {
            background-color: #04AA6D;
            color: white;
        }

        div {
            padding: 70px;
            border: 1px solid grey;
            border-radius: 5px;
        }

    </style>
</head>

<body>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <h1>BOLETO ELECTRÓNICO</h1>
    @foreach ($boleto as $boleta)
        <div>
            <table>
                <thead>
                    <tr>
                        <th>N° Boleto</th>
                        <th>Pasajero</th>
                        <th>N° Asiento</th>
                        <th>Servicio de bus</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $boleta->id }}</td>
                        <td>{{ $boleta->nombre }} {{ $boleta->apellido }}</td>
                        <td>{{ $boleta->num_asiento }}</td>
                        <td>{{ $boleta->tipo }}</td>
                    </tr>
                </tbody>
            </table><br>

            <table>
                <thead>
                    <tr>
                        <th>Origen: {{ $boleta->origen }}</th>
                        <th>Destino: {{ $boleta->destino }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Fecha de partida: {{ $boleta->fecha_partida }}</td>
                        <td>Hora de partida: {{ $boleta->hora_partida }}</td>
                    </tr>
                </tbody>
            </table>
        </div><br>
    @endforeach
</body>

</html>
