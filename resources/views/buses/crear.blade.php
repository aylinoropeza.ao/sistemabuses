
<div class="container">
    <div class="card">
        <h4>Registrar buses</h4>
        <form class="row g-3">
            <div class="col-md-6">
              <label for="emp" class="form-label">Empresa</label>
              <input type="text" name="empresa" class="form-control" id="emp">
            </div>
            <div class="col-md-6">
              <label for="cantAs" class="form-label">Cantidad de asientos</label>
              <input type="number" name="cant_asientos" class="form-control" id="cantAs">
            </div>
            <div class="col-12">
              <label for="placa" class="form-label">Placa</label>
              <input type="text" name="placa" class="form-control" id="placa">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="tipo">Tipo</label>
                <select class="form-select" name="tipo" id="tipo">
                  <option selected>Seleccionar...</option>
                  <option value="normal">Normal</option>
                  <option value="cama">Cama</option>
                  <option value="semi cama">Semi Cama</option>
                </select>
              </div>
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </form>
    </div>
</div>
