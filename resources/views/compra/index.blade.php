@extends('template.background')
@section('contenido2')
<div class="mi-bg container-fluid">
    <div class="row ml-5">
        <h2 class="col">Detalle</h2>
    </div>
    <form action="/boleto/{{ $detalles->first()->venta_id }}" method="GET">
        @csrf
        @foreach ($detalles as $detalle)
        <div class="row ml-5 mr-5">
            <div class="card col shadow">
                <div class="card-body row">
                   <div class="col-3">
                      <h3>Pasajero</h3>
                      <p  class="h4 text-dark"><small>{{ $detalle->nombre }} {{ $detalle->apellido }}</small></p>
                   </div>
                   <div class="col-3">
                      <h3>N° de boleto</h3>
                      <p  class="h4 text-dark"><small>{{ $detalle->id }}</small></p>
                   </div>
                   <div class="col-2">
                      <h3>Origen</h3>
                      <p  class="h4 text-dark"><small>{{ $detalle->origen }}</small></p>
                   </div>
                   <div class="col-2">
                      <h3>Destino</h3>
                      <p  class="h4 text-dark"><small>{{ $detalle->destino }}</small></p>
                   </div>
                   <div class="col-2">
                      <h3>Asiento</h3>
                      <p  class="h4 text-dark"><small>{{ $detalle->num_asiento }}</small></p>
                   </div>
                </div>
             </div>
        </div>
        @endforeach
        <div class="row ml-5 mr-5">
            <div class="card col shadow">
                <div class="card-body row">
                    <div class="offset-8 col-2">
                        <h3>Precio total</h3>
                        <p  class="h4 text-dark"><small>{{ $detalle->precio * sizeof($detalles) }}</small></p>
                    </div>
                    <input type="hidden" name="precio" value="{{ $detalle->precio * sizeof($detalles) }}">
                    <div class="col-2">
                        <button type="submit" class="btn btn-success">Pagar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
