@extends('template.background')
@section('contenido2')
    <div class="mi-bg container-fluid">
        {{-- <div class="row"> --}}
            <form id="form" action="/compra" method="POST" class="row">
                @csrf
                <div class="col-4 offset-md-1">
                    <div class="card shadow">
                        <div class="card-body m-3">
                            <h1 class="card-title">Selección de asiento</h5>
                            <div class="m-4">
                                @for ($i = 1; $i <= $espacios->first()->n_filas; $i++)
                                @for ($j = 1; $j <= $espacios->first()->n_columnas; $j++)
                                    @foreach ($espacios as $espacio)
                                        @if ($espacio->fila == $i && $espacio->columna == $j)
                                            @if ($espacio->estado == 'ocupado')
                                                <button type="button" class='asiento btn btn-lg m-2' id="{{ $espacio->id }}"
                                                    disabled>{{ $espacio->num_asiento }}</button>
                                            @else
                                                <button type="button" onclick="Seleccionar({{ $espacio->id }});"
                                                    class='asiento btn btn-lg m-2'
                                                    id="{{ $espacio->id }}">{{ $espacio->num_asiento }}</button>
                                            @endif
                                        @endif
                                    @endforeach
                                @endfor
                                <br>
                            @endfor
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="espacio" name="espacio" value="">
                <input type="hidden" id="venta" name="venta" value="{{ $venta->id }}">
                <input type="hidden" id="agregar" name="agregar" value="">
                <input type="hidden" id="viaje" name="viaje" value="{{ $espacios[0]->viaje_id }}">
                <div class="col-4 offset-md-1">
                    <div class="card shadow">
                        <div class="card-body m-3">
                            <h1 class="card-title">Detalle de pasajeros</h1>
                        <label class="h4"><small>Nombres</small></label>
                        <input id="nombre" name="nombre" class="form-control" type="text" placeholder="Nombre" /><br>
                        <label class="h4"><small>Apellidos</small></label>
                        <input id="apellido" name="apellido" class="form-control" type="text" placeholder="Apellidos" /><br>
                        <label class="h4"><small>CI</small></label>
                        <input id="ci" name="ci" class="form-control" type="text" placeholder="CI" /><br>
                        <label class="h4"><small>Teléfono</small></label>
                        <input id="telefono" name="telefono" class="form-control" type="text" placeholder="Teléfono" /><br>
                        <div class=''>
                            <button type="submit" onclick="Agregar()" class="btn btn-primary shadow ml-3">Añadir pasajero</button>
                            <button type="submit" class='btn btn-success shadow ml-3'>Guardar</button>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
        {{-- </div> --}}
        <div class="row">
            <form action="{{ route('compra.destroy', $venta->id) }}" method="POST" class="offset-10 col-1">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger shadow">Cancelar</button>
            </form>
        </div>
    </div>
    <script>
        function Seleccionar(idEspacio) {
            console.log(idEspacio);
            var color = document.getElementById(idEspacio).style.backgroundColor;
            var id = document.getElementById('espacio').value;
            console.log(id);
            if (id && id != idEspacio) {
                alert("Ya ha seleccionado un asiento");
            } else {
                if (color == 'green') {
                    document.getElementById(idEspacio).style.backgroundColor = '';
                    document.getElementById('espacio').value = '';
                } else {
                    document.getElementById(idEspacio).style.backgroundColor = 'green';
                    document.getElementById('espacio').value = idEspacio;
                }
            }
        }

        function Agregar() {
            document.getElementById('agregar').value = 'true';
            //document.getElementById('form').method = 'GET';
            //document.getElementById('form').action = document.getElementById('espacio').value+'/edit';
            //document.getElementById('a').href = document.getElementById('espacio').value+'/edit';
        }

    </script>
@endsection
