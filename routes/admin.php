<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ReporteController;

Route::get('', [HomeController::class, 'index']);
// Tramo
Route::resource('/tramo', 'App\Http\Controllers\Admin\TramoController');
// Reporte
Route::get('/reporte', ReporteController::class);
//buses
Route::resource('/bus', 'App\Http\Controllers\BusController');

// viajes
Route::resource('/viajes', 'App\Http\Controllers\ViajeController');

//asientos
Route::resource('/asientos', 'App\Http\Controllers\AsientoController');

//Route::get('/bus', [BusController::class, 'index'])->name('bus');
//espacios
Route::resource('/espacios', 'App\Http\Controllers\EspacioController');
//Route::resource('/bus/crear', [BusController::class, 'create']);
