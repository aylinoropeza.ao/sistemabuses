<?php

use App\Http\Controllers\ViajeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BoletoController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\BusController;
use App\Http\Controllers\DestinosController;
use App\Http\Controllers\PagoController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::resource('buses','BusController');
Route::resource('buses', 'App\Http\Controllers\BusController');

Route::get('/', function () {
    return view('destinos.buscador');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/admin', function () {
    return view('admin.index');
})->name('admin');

//           ruta    clase controlador       método
Route::get('/home', [HomeController::class, 'index'])->name('home');
//           ruta    clase controlador       método
// Route::get('admin/viajes', [ViajeController::class, 'index'])->name('viajes');
// Route::get('/viajes/crear', [ViajeController::class, 'create']);
Route::get('admin/bus', [BusController::class, 'index'])->name('bus');
Route::get('/bus/crear', [BusController::class, 'create']);
Route::get('admin/viajes', [ViajeController::class, 'index'])->name('viajes');
// Route::get('/viajes', [ViajeController::class, 'index']);
Route::get('/viajes/crear', [ViajeController::class, 'create']);

Route::get('admin/bus', [BusController::class, 'index'])->name('bus');
Route::get('/bus/crear', [BusController::class, 'create']);
// Route::get('/bus', [BusController::class, 'index'])->name('home');
Route::get('boleto/{id}', BoletoController::class);
Route::get('/cliente', function(){ return view('cliente'); });
Route::resource('/compra', 'App\Http\Controllers\CompraController');
Route::get('pago/{id}', PagoController::class);
Route::get('/paypal/pay', [PaymentController::class, 'payWithPayPal']);
Route::get('/paypal/status', [PaymentController::class, 'payPalStatus']);

// Route::get('/destinos', function(){ return view('destinos.listar'); });
Route::get('/destinos',[DestinosController::class, 'index']);


//Auth::routes();


//Route::get('/buses','BusController@listar')->name('bus');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
