<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tramo_id');
            $table->unsignedBigInteger('bus_id');
            $table->time('hora_partida');
            $table->date('fecha_partida');
            $table->time('hora_llegada');
            $table->date('fecha_llegada');
            $table->float('precio');
            $table->foreign('tramo_id')
                ->references('id')
                ->on('tramos')
                ->onDelete('cascade')
                ->onUpdate('cascade'); 
            $table->foreign('bus_id')
                ->references('id')
                ->on('buses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viajes');
    }
}
