<?php

namespace Database\Factories;

use App\Models\Asiento;
use App\Models\Bus;
use Illuminate\Database\Eloquent\Factories\Factory;

class AsientoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Asiento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'num_asiento' => $this->faker->numerify('A##'),
            'fila' => $this->faker->randomDigit(),
            'columna' => $this->faker->randomDigit(),
            'estado' => $this->faker->randomElement(['disponible','ocupado']),
            'bus_id' => Bus::factory()
        ];
    }
}
