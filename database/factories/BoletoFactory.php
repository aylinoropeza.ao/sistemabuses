<?php

namespace Database\Factories;

use App\Models\Boleto;
use App\Models\Cliente;
use App\Models\Viaje;

use Illuminate\Database\Eloquent\Factories\Factory;

class BoletoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Boleto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cliente_id' => Cliente::factory(),
            'viaje_id' => Viaje::factory(),
            'precio' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 1000)
        ];
    }
}
