<?php

namespace Database\Factories;

use App\Models\Tramo;
use Illuminate\Database\Eloquent\Factories\Factory;

class TramoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tramo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'origen' => $this->faker
            ->randomElement(['Pando', 'Beni', 'La Paz', 'Santa Cruz', 'Cochabamba', 'Potosí', 'Oruro', 'Sucre', 'Tarija']),
            'destino' => $this->faker
            ->randomElement(['Pando', 'Beni', 'La Paz', 'Santa Cruz', 'Cochabamba', 'Potosí', 'Oruro', 'Sucre', 'Tarija']),
        ];
    }
}
