<?php

namespace Database\Factories;

use App\Models\Viaje;
use App\Models\Tramo;
use App\Models\Bus;

use Illuminate\Database\Eloquent\Factories\Factory;

class ViajeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Viaje::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'hora_partida' => $this->faker->time(),
            'fecha_partida' => $this->faker->date(),
            'hora_llegada' => $this->faker->time(),
            'fecha_llegada' => $this->faker->date(),
            'precio' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 1000),
            'tramo_id' => Tramo::factory(),
            'bus_id' => Bus::factory()
        ];
    }
}
