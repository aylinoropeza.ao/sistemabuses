<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Tramo;

class TramoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_indexTramo()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/admin/tramo');

        $response->assertStatus(200);
    }
    public function test_createTramo()
    {
        $user = User::factory()->create();
        $test_tramo = [
            'origen' => 'Sucre',
            'destino' => 'Cochabamba'
        ];
        $response = $this->actingAs($user)->post('/admin/tramo', $test_tramo);

        $tramo = Tramo::where('origen', $test_tramo['origen'])->first();
        $this->assertNotNull($tramo);

        $this->post('/logout');

        // usuario sin loguear
        $response = $this->post('/tramo', $test_tramo);
        $response->assertStatus(404);
    }
    public function test_updateTramo()
    {
        $user = User::factory()->create();
        $test_tramo = [
            'origen' => 'Sucre',
            'destino' => 'Cochabamba'
        ];
        $response = $this->actingAs($user)->post('/admin/tramo', $test_tramo);

        $test_tramo_update = [
            'origen' => 'Sucre',
            'destino' => 'La Paz'
        ];
        $response = $this->actingAs($user)->put('admin/posts/1', $test_tramo_update);

        $ddbb_tramo = Tramo::find(1);

        $this->assertEquals($ddbb_tramo['origen'], $test_tramo_update['origen']);
        $this->assertEquals($ddbb_tramo['destino'], $test_tramo_update['destino']);
    }
    public function test_deleteTramo(){

        $user = User::factory()->create();
        $response = $this->actingAs($user)->withoutExceptionHandling();
        $tramo=Tramo::factory()->create();
        $response->delete("admin/tramo/{$tramo->id}")->assertRedirect('admin/tramo');
        
        $response->assertDatabaseMissing('tramos',[
            'id'=>$tramo->id
        ]);

    }
}
