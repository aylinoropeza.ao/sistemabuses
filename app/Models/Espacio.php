<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Espacio extends Model
{
    use HasFactory;
    //Relación uno a muchos (Inverso)
    public function asiento(){
        return $this->belongsTo('App\Models\Asiento');
    }
    //Relación uno a muschos (inverso)
    public function viaje(){
        return $this->belongsTo('App\Models\Viaje');
    }
}
