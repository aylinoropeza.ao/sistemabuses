<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tramo extends Model
{
    use HasFactory;

    //Relación uno a muchos
    public function viajes(){
        return $this->hasMany('App\Models\Viaje');
    }
}
