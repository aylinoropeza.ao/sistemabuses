<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    use HasFactory;

    //Relación uno a muchos
    public function asientos(){
        return $this->hasMany('App\Models\Asiento');
    }
    //Relación uno a muchos
    public function viajes(){
        return $this->hasOne('App\Models\Viaje');
    }
}
