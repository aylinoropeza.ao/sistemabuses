<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asiento extends Model
{
    use HasFactory;

    //Relación uno a muchos (Inversa)
    public function bus(){
        return $this->belongsTo('App\Models\Bus');
    }
    //Relación uno a muchos
    public function espacios(){
        return $this->hasMany('App\Models\Espacio');
    }
}
