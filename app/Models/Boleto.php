<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boleto extends Model
{
    use HasFactory;
    //Relación uno a muchos
    public function clientes(){
        return $this->hasMany('App\Models\Cliente');
    }
    //Relación uno a muchos
    public function espacios(){
        return $this->hasMany('App\Models\Espacio');
    }
    //Relación uno a muchos (inverso)
    public function venta(){
        return $this->belongsTo('App\Models\Venta');
    }
}
