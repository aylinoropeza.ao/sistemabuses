<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;

    //Relación uno a muchos
    public function boletos(){
        return $this->hasMany('App\Models\Boleto');
    }
}
