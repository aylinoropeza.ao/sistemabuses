<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Viaje extends Model
{
    use HasFactory;
    //Relación uno a muchos (Inverso)
    public function tramo(){
        return $this->belongsTo('App\Models\Tramo');
    }
    //Relación uno a muchos (Inverso)
    public function viaje(){
        return $this->belongsTo('App\Models\Viaje');
    }
    //Relación uno a muchos
    public function espacios(){
        return $this->hasMany('App\Models\Espacio');
    }
}
