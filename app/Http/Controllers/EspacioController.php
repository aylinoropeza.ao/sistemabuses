<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Espacio;
use App\Models\Viaje;
use App\Models\Asiento;

class EspacioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idViaje =  $request->get('id');
        /**/
        $espacios = Espacio::where('viaje_id','=', $idViaje)->join('asientos', 'espacios.asiento_id', '=', 'asientos.id')->get();
        $viaje = Viaje::where('viajes.id', '=', $idViaje)->join('buses', 'viajes.bus_id', '=', 'buses.id')->get();
        return view('admin.espacios.index',[
            'espacios'=>$espacios,
            'viaje'=> $viaje[0]
         ]);
         
        // $espacios = [];
        // $viaje = Viaje::where('viajes.id', '=', $idViaje)
        //                 ->join('buses', 'viajes.bus_id', '=', 'buses.id')->get();

        // $asientos = Viaje::where('viajes.id', '=', $idViaje)
        //                 ->join('buses', 'viajes.bus_id', '=', 'buses.id')
        //                 ->join('asientos', 'asientos.bus_id', '=', 'buses.id')
        //                 // ->join('espacios', 'espacios.asiento_id', '=', 'asientos.id')
        //                 ->get();
        
        // return view('admin.espacios.index',[
        //     'espacios' => $espacios,
        //     'asientos' => $asientos,
        //     'viaje' => $viaje[0]
        //  ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
