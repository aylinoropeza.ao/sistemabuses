<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Models\Boleto;
use App\Models\Venta;
use App\Models\Asiento;
use App\Models\Espacio;



class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // Operaciones para guardar la compra
    public function store(Request $request)
    {
        //hidden
        $id_espacio = $request->get('espacio');
        $id_venta = $request->get('venta');
        $agregar = $request->get('agregar');

        // Cliente
        $cliente = new Cliente();
        $cliente->nombre = $request->get('nombre');
        $cliente->apellido = $request->get('apellido');
        $cliente->ci = $request->get('ci');
        $cliente->telefono = $request->get('telefono');
        $cliente->save();

        // Boleto
        $boleto = new Boleto();
        $boleto->cliente_id = $cliente->id;
        $boleto->espacio_id = $id_espacio;
        $boleto->venta_id = $id_venta;
        $boleto->save();

        // Actualización de estado
        $espacio = Espacio::find($id_espacio);
        $espacio->estado = 'ocupado';
        $espacio->save();

        // Agregar pasajeros
        if($agregar == 'true'){
             return redirect('compra/'.$id_espacio.'/edit');
        } else {
            return redirect('pago/'.$id_venta);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Formulario de compra
    public function show($id)
    {
        $venta = new Venta();
        $venta->save();
        $espacios = Espacio::join('asientos', 'asientos.id', '=', 'espacios.asiento_id')
            ->join('buses', 'buses.id', '=', 'asientos.bus_id')
            ->where('espacios.viaje_id', $id)
            ->select('espacios.*', 'asientos.num_asiento', 'asientos.fila', 'asientos.columna', 'buses.n_filas', 'buses.n_columnas')
            ->get()
            ->sortBy('fila')->sortBy('columna');
        return view('compra.show', compact('espacios'))->with(compact('venta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Detalle de compra
    public function edit($id)
    {
        $espacio = Espacio::find($id);
        $viaje = $espacio->viaje_id;
        $venta = Boleto::join('espacios', 'boletos.espacio_id', 'espacios.id')
            ->join('ventas', 'boletos.venta_id', 'ventas.id')->select('ventas.*')
            ->where('boletos.espacio_id', $id)->first();

        $espacios = Espacio::join('asientos', 'asientos.id', '=', 'espacios.asiento_id')
            ->join('buses', 'buses.id', '=', 'asientos.bus_id')
            ->where('espacios.viaje_id', $viaje)
            ->select('espacios.*', 'asientos.num_asiento', 'asientos.fila', 'asientos.columna', 'buses.n_filas', 'buses.n_columnas')
            ->get()
            ->sortBy('fila')->sortBy('columna');
        return view('compra.show', compact('espacios'))->with(compact('venta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Eliminar registro de venta en caso de cancelar compra
    public function destroy($id)
    {
        $venta = Venta::find($id);
        $venta->delete();
        return redirect('/');
    }
}
