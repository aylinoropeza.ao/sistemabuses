<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Boleto;
use Barryvdh\DomPDF\PDF as DomPDFPDF;
use PDF;

class BoletoController extends Controller
{
    public function __invoke($id)
    {
        $boleto = Boleto::join('ventas', 'boletos.venta_id', 'ventas.id')
            ->join('clientes', 'boletos.cliente_id', 'clientes.id')
            ->join('espacios', 'boletos.espacio_id', 'espacios.id')
            ->join('asientos', 'espacios.asiento_id', 'asientos.id')
            ->join('buses', 'asientos.bus_id', 'buses.id')
            ->join('viajes', 'espacios.viaje_id', 'viajes.id')
            ->join('tramos', 'viajes.tramo_id', 'tramos.id')
            ->where('ventas.id', $id)
            ->select('boletos.*', 'clientes.nombre', 'clientes.apellido', 'asientos.num_asiento', 'buses.tipo', 'tramos.origen', 'tramos.destino', 'viajes.hora_partida', 'viajes.fecha_partida')
            ->get();

        //return view('boleto', compact('boleto'));
        $pdf = PDF::loadview('boleto', compact('boleto'))->setOptions(['defaultFont' => 'sans-serif']);
        return $pdf->stream('boletoBus.pdf');
    }
}
