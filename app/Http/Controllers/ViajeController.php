<?php

namespace App\Http\Controllers;

use App\Models\Viaje;
use Illuminate\Http\Request;
use App\Models\Tramo;
use App\Models\Bus;
use App\Models\Espacio;

class ViajeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $viajes = Viaje::all();
        foreach ($viajes as $key => $value) {
            $busActual = Bus::where('id', $value->bus_id )->first();
            $viajes[$key]["bus"] = $busActual->placa;
        }
        
        foreach ($viajes as $key => $value) {
            $tramoActual = Tramo::where('id', $value->tramo_id )->first();
            $viajes[$key]["tramo"] = $tramoActual->origen .' - '.$tramoActual->destino;
        }
        
        return view('admin.viajes.listar', [
            "viajes"=> $viajes,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $tramos = Tramo::all();
        $buses = Bus::all();
        return view('admin.viajes.crear',
            ['tramos' => $tramos],
            ['buses' => $buses]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $viaje = new Viaje();

        $viaje->hora_partida = $request->get('hora_partida');
        $viaje->fecha_partida = $request->get('fecha_partida');
        $viaje->tramo_id = $request->get('tramo_id');
        $viaje->bus_id = $request->get('bus_id');
        $viaje->precio = $request->get('precio');
        $viaje->hora_llegada = $request->get('hora_partida');
        $viaje->fecha_llegada = $request->get('fecha_partida');
        $viaje->save();
        
        $idViaje = $viaje->id;
        $idBus = $request->get('bus_id');
        $busAsientos = Bus::where('buses.id','=',$idBus)
                    ->join('asientos', 'asientos.bus_id', '=', 'buses.id')
                    ->select('asientos.id')
                    ->get();

        foreach ($busAsientos as $asiento) {
            $espacio = new Espacio();
            $espacio->estado="disponible";
            $espacio->asiento_id = $asiento->id;
            $espacio->viaje_id = $idViaje;
            $espacio->save();
        } 
        return redirect('/admin/viajes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Viaje  $viaje
     * @return \Illuminate\Http\Response
     */
    public function show(Viaje $viaje)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Viaje  $viaje
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $viaje = Viaje::find($id);
        $tramos = Tramo::all();
        $buses = Bus::all();
        return view('admin.viajes.edit', )
            ->with('viaje', $viaje)
            ->with('tramos', $tramos)
            ->with('buses', $buses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Viaje  $viaje
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $viaje = Viaje::find($id);

        $viaje->hora_partida = $request->get('hora_partida');
        $viaje->fecha_partida = $request->get('fecha_partida');
        $viaje->tramo_id = $request->get('tramo_id');
        $viaje->bus_id = $request->get('bus_id');
        $viaje->precio = $request->get('precio');
        $viaje->save();

        return redirect('/admin/viajes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Viaje  $viaje
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $viaje = Viaje::find($id);
        $viaje->delete();
        return redirect('/admin/viajes'); 
    }
}
