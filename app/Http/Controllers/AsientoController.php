<?php

namespace App\Http\Controllers;

use App\Models\Asiento;
use Illuminate\Http\Request;

class AsientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idbus = $request->get('idbus');
        $asientos=Asiento::where('bus_id','=', $idbus)->get();
         return view('admin.asientos.listar',[
            'asientos'=>$asientos
         ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.asientos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $asiento = new Asiento();
        $asiento->num_asiento = $request->get('num_asiento');
        $asiento->fila = $request->get('fila');
        $asiento->columna = $request->get('columna');
        $asiento->bus_id = $request->get('bus_id');
        $asiento->save();

        return redirect('/admin/asientos?idbus='.$request->get('bus_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Asiento  $asiento
     * @return \Illuminate\Http\Response
     */
    public function show(Asiento $asiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Asiento  $asiento
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        
        $asiento = Asiento::find($id);
        return view('admin.asientos.edit')->with('asiento', $asiento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asiento  $asiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asiento = Asiento::find($id);

        $asiento->num_asiento = $request->get('num_asiento');
        $asiento->fila = $request->get('fila');
        $asiento->columna = $request->get('columna');
        $asiento->bus_id = $request->get('bus_id');
        $asiento->save();

        return redirect('/admin/asientos?idbus='. $request->get('bus_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asiento  $asiento
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        $asiento = Asiento::find($id);
        $asiento->delete();
        return redirect('/admin/asientos?bus='. $request->get('bus_id'));
    }
}
