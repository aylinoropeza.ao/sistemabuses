<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Boleto;

class HomeController extends Controller
{
    public function index(){
        $detalles = Boleto::join('ventas', 'boletos.venta_id', 'ventas.id')
            ->join('clientes', 'boletos.cliente_id', 'clientes.id')
            ->join('espacios', 'boletos.espacio_id', 'espacios.id')
            ->join('asientos', 'espacios.asiento_id', 'asientos.id')
            ->join('buses', 'asientos.bus_id', 'buses.id')
            ->join('viajes', 'espacios.viaje_id', 'viajes.id')
            ->join('tramos', 'viajes.tramo_id', 'tramos.id')
            ->select('boletos.venta_id', 'boletos.id', 'clientes.nombre', 'clientes.apellido', 'asientos.num_asiento', 'tramos.origen', 'tramos.destino', 'buses.tipo', 'buses.placa', 'viajes.hora_partida', 'viajes.fecha_partida', 'viajes.precio')
            ->get();
        return view('admin.index', compact('detalles'));
    }
}

