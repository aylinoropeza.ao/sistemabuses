<?php

namespace App\Http\Controllers;

use App\Models\Destinos;
use App\Models\Tramo;
use App\Models\Viaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DestinosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )    {
        
        $fecha = $request->get('fecha');
        $results = Viaje::where('fecha_partida', $fecha)
                    ->join('tramos', 'viajes.tramo_id', '=', 'tramos.id')
                    ->join('buses', 'viajes.bus_id', '=', 'buses.id')
                    ->select('viajes.*', 'tramos.origen', 'tramos.destino', 'buses.tipo', 'buses.placa')
                    ->get();
        return view('destinos.listar',
            ["resultados" => $results]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function show(Destinos $destinos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function edit(Destinos $destinos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Destinos $destinos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Destinos  $destinos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destinos $destinos)
    {
        //
    }
}
