<?php

namespace App\Http\Controllers;

use App\Models\Boleto;
use Illuminate\Http\Request;

class PagoController extends Controller
{
    public function __invoke($id)
    {
        $detalles = Boleto::join('ventas', 'boletos.venta_id', 'ventas.id')
        ->join('clientes', 'boletos.cliente_id', 'clientes.id')
        ->join('espacios', 'boletos.espacio_id', 'espacios.id')
        ->join('asientos', 'espacios.asiento_id', 'asientos.id')
        ->join('buses', 'asientos.bus_id', 'buses.id')
        ->join('viajes', 'espacios.viaje_id', 'viajes.id')
        ->join('tramos', 'viajes.tramo_id', 'tramos.id')
        ->where('ventas.id', $id)->get();

        //dd($detalle);
        return view('compra.index', compact('detalles'));
    }
    
}
